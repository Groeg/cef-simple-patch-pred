// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include "tests/cefsimple/simple_handler.h"

#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <string>

#include "include/base/cef_logging.h"
#include "include/cef_browser.h"

void SimpleHandler::PlatformTitleChange(CefRefPtr<CefBrowser> browser,
                                        const CefString& title) {
  std::string titleStr(title);

  // Retrieve the X11 display shared with Chromium.
  ::Display* display = cef_get_xdisplay();
  DCHECK(display);

  // Retrieve the X11 window handle for the browser.
  ::Window window = browser->GetHost()->GetWindowHandle();
  DCHECK(window != kNullWindowHandle);

  // Retrieve the atoms required by the below XChangeProperty call.
  const char* kAtoms[] = {
    "_NET_WM_NAME",
    "UTF8_STRING"
  };
  Atom atoms[2];
  int result = XInternAtoms(display, const_cast<char**>(kAtoms), 2, false,
                            atoms);
  if (!result)
    NOTREACHED();

  // Set the window title.
  XChangeProperty(display,
                  window,
                  atoms[0],
                  atoms[1],
                  8,
                  PropModeReplace,
                  reinterpret_cast<const unsigned char*>(titleStr.c_str()),
                  titleStr.size());

  // TODO(erg): This is technically wrong. So XStoreName and friends expect
  // this in Host Portable Character Encoding instead of UTF-8, which I believe
  // is Compound Text. This shouldn't matter 90% of the time since this is the
  // fallback to the UTF8 property above.
  XStoreName(display, browser->GetHost()->GetWindowHandle(), titleStr.c_str());


  // Putting the window in fullscreen mode
  Atom wm_state = XInternAtom(display, "_NET_WM_STATE", 0);
  Atom fullscreen = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", 0);

  XClientMessageEvent xcmev = { 0 };
  xcmev.type = ClientMessage;
  xcmev.window = window;
  xcmev.message_type = wm_state;
  xcmev.format = 32;
  xcmev.data.l[0] = 1;
  xcmev.data.l[1] = fullscreen;

  Window root = DefaultRootWindow(display);

  XSendEvent(display,
	         root,
	         0,
	         (SubstructureRedirectMask | SubstructureNotifyMask),
	         (XEvent*)&xcmev);


  // Code from the SDL2 codebase whose purpose is to make the window borderless
  Atom WM_HINTS = XInternAtom(display, "_MOTIF_WM_HINTS", True);
  if (WM_HINTS != None) {
    /* Hints used by Motif compliant window managers */
    struct{
      unsigned long flags;
      unsigned long functions;
      unsigned long decorations;
      long input_mode;
      unsigned long status;
    } MWMHints = {
      (1L << 1), 0, False ? 1 : 0, 0, 0
    };

    XChangeProperty(display, window, WM_HINTS, WM_HINTS, 32,
                    PropModeReplace, (unsigned char *) &MWMHints,
                    sizeof(MWMHints) / sizeof(long));
  } else {  /* set the transient hints instead, if necessary */
    XSetTransientForHint(display, window, RootWindow(display, 0));
  }
  // End of code from SDL2

  // Sending first console.log
  CefRefPtr<CefFrame> frame = browser->GetMainFrame();
  frame->ExecuteJavaScript("console.log('Get_VIDEO_C2PLAYER_div_position')", frame->GetURL(), 0);

}

